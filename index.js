const width = 100;
const height = 50;
const cellSize = 11;
const directions = ['Left', 'Up', 'Right', 'Down'];
const board = [];
const cellStates = [];

const state = {
    x: Math.ceil(width / 2),
    y: Math.ceil(height / 2),
    direction: 1,
    on: 0,
    off: 0,
    frame: 0,
}


document.addEventListener('readystatechange', e => {
    if (document.readyState != 'complete') {
        return;
    }
    const divBoard = document.getElementById('board');
    divBoard.style.width = `${cellSize * width}px`;
    divBoard.style.height = `${cellSize * height}px`;
    for (let y = 0; y < height; y++) {
        const line = [];
        cellStates[y] = [];
        for (let x = 0; x < width; x++) {
            const cell = document.createElement('div');
//             cell.setAttribute('data-x', x);
//             cell.setAttribute('data-y', y);
            if (state.x === x && state.y === y) {
                cell.classList.add(`Ant${directions[state.direction]}`);
            }
            divBoard.appendChild(cell);
            line.push(cell);
            cellStates[y][x] = false;
            state.off++;
        }
        board.push(line);
    }
    const spnActualFramerate = createStat('Actual fps');
    const spnX = createStat('X');
    const spnY = createStat('Y');
    const spnDirection = createStat('Direction');
    const spnOns = createStat('On');
    const spnOffs = createStat('Off');
    const spnFrame = createStat('Frame');
    createSeedPattern('Clear', (x, y) => false);
    createSeedPattern('All On', (x, y) => true);
    createSeedPattern('Columns', (x, y) => !!(x % 2));
    createSeedPattern('Checker board', (x, y) => !!((x % 2) ^ (y % 2)));
    createSeedPattern('Random', (x, y) => !!(Math.round(Math.random())));
    const smiley =  ['0000000000000', 
    '0000111110000', 
    '0001000001000', 
    '0010000000100', 
    '0100100010010', 
    '0100100010010',
    '0100000000010',
    '0100000000010',
    '0100100010010',
    '0010011100100',
    '0001000001000',
    '0000111110000',
    '0000000000000'
 ];
    createSeedPattern('Smiley', (x, y) =>smiley[Math.min(smiley.length-1,    Math.max(0, Math.floor((y - height/2) + (smiley.length   /2))))]
                                               [Math.min(smiley[0].length-1, Math.max(0, Math.floor((x - width /2) + (smiley[0].length/2))))]==='1');


    let lastUpdate = 0;
    let updateTimer;
    let framesAtLastUpdate=0;
    function updateInfo(state) {
        if (new Date() - lastUpdate < 200) {
            updateTimer = setTimeout(() => updateInfo(state), 50);
            return;
        }
        clearTimeout(updateTimer);
        const fps = (state.frame - framesAtLastUpdate) / (new Date() - lastUpdate) * 1000;
        framesAtLastUpdate = state.frame;
        lastUpdate = new Date();
        spnActualFramerate.innerText = fps.toFixed(1);
        spnX.innerText = state.x;
        spnY.innerText = state.y;
        spnDirection.innerText = directions[state.direction];
        spnOns.innerText = state.on;
        spnOffs.innerText = state.off;
        spnFrame.innerText = state.frame;
    }
    updateInfo(state);
    document.body.addEventListener('keyup', ()=>runFrame(true));

    function createStat(name) {
        const ulInfo = document.getElementById('Info');
        const li = document.createElement('li');
        const lbl = document.createElement('label');
        const spn = document.createElement('span');
        lbl.innerText = name;
        li.appendChild(lbl);
        li.appendChild(spn);
        ulInfo.appendChild(li);
        return spn;
    }

    const inpSpeed = document.getElementById('Speed');
    inpSpeed.addEventListener('change', () => {
        inpSpeed.nextElementSibling.innerText = `${(inpSpeed.value / 10).toFixed(1)}fps`
    });
    inpSpeed.nextElementSibling.innerText = `${(inpSpeed.value / 10).toFixed(1)}fps`
    let frameTimeout;
    let lastFrame=new Date();
    function runFrame(skipFrameCheck) {
        if (!skipFrameCheck){
          console.log('time since last frame:', new Date() - lastFrame, inpSpeed.value/10);
          let framesToRun =  Math.floor(Math.min(1000, (new Date() - lastFrame) * inpSpeed.value /10000));
          console.log('frames to run', framesToRun);
          while (framesToRun >1) {
              runFrame(true);
              framesToRun--;
          }
        }
        lastFrame = new Date();
        if (new Date() )
        state.frame++;
        // console.log('run frame');
        if (cellStates[state.y][state.x]) {
            state.direction = ((state.direction + 1) % 4);
            state.off++;
            state.on--;
        } else {
            state.direction = (state.direction + 3) % 4;
            state.on++;
            state.off--;
        }
        cellStates[state.y][state.x] = !cellStates[state.y][state.x];
        board[state.y][state.x].classList.remove('AntUp');
        board[state.y][state.x].classList.remove('AntDown');
        board[state.y][state.x].classList.remove('AntLeft');
        board[state.y][state.x].classList.remove('AntRight');
        board[state.y][state.x].classList.toggle('On');
        if (state.direction === 0) {
            state.x = (state.x + (width - 1)) % width;
        } else if (state.direction === 1) {
            state.y = (state.y + (height - 1)) % height;
        } else if (state.direction === 2) {
            state.x = (state.x + 1) % width;
        } else if (state.direction === 3) {
            state.y = (state.y + 1) % height;
        }
        board[state.y][state.x].classList.add(`Ant${directions[state.direction]}`);
        // console.log(state);
        updateInfo(state);
        clearInterval(frameTimeout);
        if (inpSpeed.value != 0) {
            // console.log(inpSpeed.value, 10000/ inpSpeed.value)
            frameTimeout = setTimeout(runFrame, 10000 / inpSpeed.value);
            console.log('next frame in ', 10000 / inpSpeed.value);
        }
    }

    function createSeedPattern(name, pattern) {
        const ulInfo = document.getElementById('Info');
        const divButton = document.createElement('button');
        divButton.innerText = name;
        divButton.addEventListener('click', () => {
            state.on = state.off = 0;
            for (let y = 0; y < height; y++) {
                for (let x = 0; x < width; x++) {
                    cellStates[y][x] = pattern(x, y);
                    console.log(cellStates[y][x]);
                    if (cellStates[y][x]) {
                        board[y][x].classList.add('On');
                        state.on++;
                    } else {
                        board[y][x].classList.remove('On');
                        state.off++;
                    }
                }
            }
            updateInfo(state);
        });
        ulInfo.appendChild(divButton);
    }
});